package br.edu.up.whatchatservidor;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServidorActivity extends AppCompatActivity {

  WifiManager wifiManager;
  EditText txtChat;
  EditText txtMensagem;
  boolean isConectado = true;

  String mensagemEnviar;
  String mensagemRecebida;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_servidor);

    wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
    int numero = wifiManager.getConnectionInfo().getIpAddress();
    String txtIpAddress = Formatter.formatIpAddress(numero);

    TextView txtServidor = (TextView) findViewById(R.id.txtServidor);
    txtServidor.setText(txtIpAddress);

    txtChat = (EditText) findViewById(R.id.txtChat);
    txtMensagem = (EditText) findViewById(R.id.txtMensagem);

    new Thread(new ServidorThread()).start();

  }

  public void onClickEnviar(View v){
    String mensagem = txtMensagem.getText().toString();
    txtChat.append("\nServidor: " + mensagem);
  }

  public class ServidorThread implements Runnable {

    static final int PORTA = 8080;

    @Override
    public void run() {

      ServerSocket serverSocket = null;
      try {
        serverSocket = new ServerSocket(PORTA);

        while (isConectado) {

          Socket socket = null;
          DataOutputStream dos = null;
          DataInputStream dis = null;

          try {
            socket = serverSocket.accept();

            dis = new DataInputStream(socket.getInputStream());
            try {
              mensagemRecebida = dis.readUTF();
            } catch (IOException e){
              //Ignora;
              mensagemRecebida = null;
            }
            Log.d("Servidor", "mensagemRecebida: " + mensagemRecebida);
            if (null != mensagemRecebida && !"".equals(mensagemRecebida)) {
              runOnUiThread(new Runnable() {
                @Override
                public void run() {
                  txtChat.append("\nCliente: " + mensagemRecebida);
                  mensagemRecebida = null;
                }
              });
            }

            runOnUiThread(new Runnable() {
              @Override
              public void run() {
                mensagemEnviar = txtMensagem.getText().toString();
                txtMensagem.setText("");
              }
            });

            dos = new DataOutputStream(socket.getOutputStream());
            Log.d("Servidor", "mensagemEnviar: " + mensagemEnviar);
            if (null != mensagemEnviar && !"".equals(mensagemEnviar)) {
              dos.writeUTF(mensagemEnviar);
            }
          } catch (IOException e) {
            e.printStackTrace();
          } finally {

            if (dos != null) {
              try {
                dos.close();
              } catch (IOException e) {
                e.printStackTrace();
              }
            }

            if (dis != null) {
              try {
                dis.close();
              } catch (IOException e) {
                e.printStackTrace();
              }
            }

            if (socket != null) {
              try {
                socket.close();
              } catch (IOException e) {
                e.printStackTrace();
              }
            }
          }
        }
      } catch (Exception e){
        e.printStackTrace();
      } finally {
        if (serverSocket != null){
          try {
            serverSocket.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    isConectado = false;
  }
}
